-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 03, 2021 at 10:46 PM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED DEFAULT NULL,
  `message` text NOT NULL,
  `createdAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `userId`, `message`, `createdAt`) VALUES
(519, 297, '11', '2021-05-03 23:22:11'),
(520, 298, '2', '2021-05-03 23:22:15'),
(521, 299, '3', '2021-05-03 23:22:20'),
(522, 300, '4', '2021-05-03 23:22:22'),
(523, 300, '444', '2021-05-03 23:22:33'),
(524, 301, '444', '2021-05-03 23:22:50'),
(525, 301, '444', '2021-05-03 23:22:54'),
(526, 301, 'e', '2021-05-03 23:22:59'),
(527, 301, 'wwwwww', '2021-05-03 23:23:02'),
(528, 302, '111111111', '2021-05-03 23:23:09'),
(529, 303, 'dddddd', '2021-05-03 23:23:12'),
(530, 304, 'a', '2021-05-03 23:31:46'),
(531, 304, 'ddd', '2021-05-03 23:31:51'),
(532, 304, 'd', '2021-05-03 23:32:20'),
(533, 307, 'a', '2021-05-03 23:34:45'),
(534, 308, 'd', '2021-05-03 23:36:07'),
(535, 307, 'a', '2021-05-03 23:36:53'),
(536, 308, 'd', '2021-05-03 23:40:08'),
(537, 309, 'hyfhf', '2021-05-03 23:42:20'),
(538, 307, 'dddd', '2021-05-03 23:42:46'),
(539, 307, 'd', '2021-05-03 23:42:51'),
(540, 307, 's', '2021-05-03 23:42:54'),
(541, 307, 'eee', '2021-05-03 23:42:58'),
(542, 307, '22', '2021-05-03 23:43:03'),
(543, 307, '4444', '2021-05-03 23:43:08'),
(544, 307, '6666', '2021-05-03 23:43:14'),
(545, 310, '2', '2021-05-03 23:43:45'),
(546, 310, '3', '2021-05-03 23:43:53'),
(547, 311, 'd', '2021-05-03 23:44:50'),
(548, 311, '2', '2021-05-03 23:44:54'),
(549, 311, '5555', '2021-05-03 23:44:57'),
(550, 311, '88888888', '2021-05-03 23:45:08'),
(551, 311, 'bbbbbbbb', '2021-05-03 23:45:41'),
(552, 311, '222', '2021-05-03 23:45:47'),
(553, 311, '1111', '2021-05-03 23:45:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `nickName` varchar(40) NOT NULL,
  `lastActive` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nickName`, `lastActive`) VALUES
(297, '11', '2021-05-03 23:22:11'),
(298, '222', '2021-05-03 23:22:15'),
(299, '3', '2021-05-03 23:22:20'),
(300, '4', '2021-05-03 23:22:33'),
(301, '44444', '2021-05-03 23:23:02'),
(302, '1111', '2021-05-03 23:23:09'),
(303, 'ddddddd', '2021-05-03 23:23:12'),
(304, 'Array', '2021-05-03 23:31:46'),
(305, 'Array', '2021-05-03 23:31:51'),
(306, 'Array', '2021-05-03 23:32:20'),
(307, 'aaa', '2021-05-03 23:43:14'),
(308, 'd', '2021-05-03 23:40:08'),
(309, 'ee', '2021-05-03 23:42:20'),
(310, '2', '2021-05-03 23:43:53'),
(311, 'dd', '2021-05-03 23:45:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test` (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=554;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `test` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
