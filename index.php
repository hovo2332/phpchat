<?php

	require_once 'connect.php';

	if (isset($_POST['start'])) {
		$join =  mysqli_query($conn, "SELECT *, messages.id AS message_id, users.id AS user_id FROM messages INNER JOIN users ON users.id = messages.userId");
		$arr =  mysqli_fetch_all($join, MYSQLI_ASSOC);
		
		$getUsers = mysqli_query($conn, "SELECT * FROM users WHERE users.lastActive > DATE_SUB(NOW(), INTERVAL 15 MINUTE)");
		$usersArr = mysqli_fetch_all($getUsers, MYSQLI_ASSOC);
		
		echo json_encode(['users' => $usersArr, 'messages' => $arr]); 
		exit;
	}


	if (isset($_POST['nickName']) && isset($_POST['message']) && isset($_POST['lastMessageId'])) {
		
		$nickName = $_POST['nickName'];
		$message = $_POST['message'];
		$lastMessageId = $_POST['lastMessageId'];
		$now = date("Y-m-d H:i:s");

		$selectNickname = mysqli_query($conn,"SELECT nickName FROM users WHERE nickName = '$nickName'");
		$nickNameArr = mysqli_fetch_all($selectNickname, MYSQLI_ASSOC);
    
		if (count($nickNameArr) === 0) {
			mysqli_query($conn, "INSERT INTO users (nickName, lastActive) VALUES ('$nickName', '$now')");
			$getUsers = mysqli_query($conn, "SELECT * FROM users WHERE nickName = '$nickName'");
			$usersArr = mysqli_fetch_all($getUsers, MYSQLI_ASSOC);
		} else {
      mysqli_query($conn, "UPDATE users SET lastActive = '$now' WHERE users.nickName = '$nickName'");
    }
		

		$getUserId = mysqli_query($conn, "SELECT id FROM users WHERE nickName = '$nickName'");
		$userId =  mysqli_fetch_assoc($getUserId)['id'];

		mysqli_query($conn, "INSERT INTO messages (message, createdAt, userId) VALUES ('$message', '$now', '$userId')");

		$join =  mysqli_query($conn, "SELECT *, messages.id AS message_id, users.id AS user_id FROM messages INNER JOIN users ON users.id = messages.userId WHERE messages.id > '$lastMessageId'");
		$arr =  mysqli_fetch_all($join, MYSQLI_ASSOC);
		

		$getUsers = mysqli_query($conn, "SELECT * FROM users WHERE users.lastActive > DATE_SUB(NOW(), INTERVAL 15 MINUTE)");
		$usersArr = mysqli_fetch_all($getUsers, MYSQLI_ASSOC);

		echo json_encode(['users' => $usersArr, 'messages' => $arr]); 
		
		exit;
	}
	

	if (isset($_POST['lastMessageId'])) {

		$lastMessageId = $_POST['lastMessageId'];

		
		$join =  mysqli_query($conn, "SELECT *, messages.id AS message_id, users.id AS user_id FROM messages INNER JOIN users ON users.id = messages.userId WHERE messages.id > '$lastMessageId'");
		$arr =  mysqli_fetch_all($join, MYSQLI_ASSOC);
		
		
		$getUsers = mysqli_query($conn, "SELECT * FROM users WHERE users.lastActive > DATE_SUB(NOW(), INTERVAL 15 MINUTE)");
		$usersArr = mysqli_fetch_all($getUsers, MYSQLI_ASSOC);
		
		echo json_encode(['users' => $usersArr, 'messages' => $arr]);	

		exit;
	}

	mysqli_close($conn);
?>




<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="wrapper">
    <div class="messages">
      <p class="messages__title title">MESSAGES</p>
			<div class="messages__preview"></div>
			<div class="messagesSend">
				<input type="text" class="usernameInp" name="nickName" placeholder="Username">
				<textarea name="message" cols="45" rows="2" placeholder="Type a message ..." class="messageArea"></textarea>
				<button class="messagesBtn" name="sendMessage">Send Message</button>
			</div>
		</div>
    <div>
      <p class="users__title title">Active Users</p>
      <div class="users">
      </div>
    </div>
	</div>

	
	<script src="js/script.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>