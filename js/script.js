const messagesBtn = document.querySelector('.messagesBtn');
const usernameInp = document.querySelector('.usernameInp');
const messageInp = document.querySelector('.messageArea');
const preview = document.querySelector('.messages__preview');
const showUsers = document.querySelector('.users');

function getAll() {

  $.ajax({
    url: 'index.php',
    method: "POST",
    dataType: "json",
    data: {
      start: 'start'
    },
    success: function (response) {
      if (response.messages.length > 0) {
        response.messages.forEach(message => {
          drawMessages(message)
        });
      }
      if (response.users.length > 0) {
        showUsers.innerHTML = '';
        response.users.forEach(user => {
          drawUsers(user)
        });
      }
    }
  });
}

window.addEventListener('DOMContentLoaded', getAll);


function send() {
  const nickName = usernameInp.value;
  const message = messageInp.value;

  let messages = document.querySelectorAll('.info__message');
  let lastMessageId;

  if (messages.length === 0) {
    lastMessageId = 0;
  } else {
    lastMessageId = messages[messages.length - 1].dataset.id;
  }

  if (!message.trim() || !nickName.trim()) {
    alert('Fill all fields !');
    return;
  }

  $.ajax({
    url: 'index.php',
    method: "POST",
    dataType: "json",
    data: {
      nickName,
      message,
      lastMessageId,
    },
    success: function (response) {
      if (response.messages.length > 0) {
        response.messages.forEach(message => {
          drawMessages(message)
        })
      }
      if (response.users.length > 0) {
        showUsers.innerHTML = '';
        response.users.forEach(user => {
          drawUsers(user)
        })
      }
      preview.scrollTop = preview.scrollHeight;
    }
  });
  messageInp.value = '';
}

messagesBtn.addEventListener('click', send)

function update() {

  let messages = document.querySelectorAll('.info__message');
  let lastMessageId;

  if (messages.length === 0) {
    lastMessageId = 0;
  } else {
    lastMessageId = messages[messages.length - 1].dataset.id;
  }

  $.ajax({
    url: 'index.php',
    method: "POST",
    dataType: "json",
    data: {
      lastMessageId,
    },
    success: function (response) {
      if (response.messages.length > 0) {
        response.messages.forEach(message => {
          drawMessages(message)
        })
      }
      showUsers.innerHTML = '';
      if (response.users.length > 0) {
        response.users.forEach(user => {
          drawUsers(user)
        })
      }
    }
  });
}

setInterval(update, 5000);


function drawUsers(element) {

  showUsers.innerHTML +=
    `
        <div class="info">
            <p class="userInfo" data-id="${element.id}">${element.nickName}</p>
        </div>
    `
}

function drawMessages(element) {
  preview.innerHTML +=
    `
       <div class="info">
          <p class="info__name">${element.nickName}</p>
          <span class="info__message" data-id="${element.message_id}">${element.message}</span>
          <span class="createdAt">${element.createdAt}</span>
       </div>
    `
}




